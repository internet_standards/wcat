import pycurl
from urllib.parse import urlunparse
try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO

'''Class to be used to store each standards to be tested and the results'''
class ConnResult:
    def __init__(self):
        self.error = None

def debug_result(result):
    if result.error is None:
        print('Status: %d' % result.response_code)
        print('HTTP Version: %s' % result.http_version)
        print('Time: %f' % result.total_time)
        print("Headers: %s" % result.headers)
        print("Content: %s" % result.content)
    else:
        print("ERROR: %s" % result.error)

'''Sends a get request using the requested scheme and protocol'''
'''Returns (response object, error). Error is None is the request was succesfull'''
def run_http_check (domain, user_agent, scheme="https", http_version = 1, ipv6 = False):
    result = ConnResult()
    headers = {}
    def header_function(header_line):
        # HTTP standard specifies that headers are encoded in iso-8859-1.
        header_line = header_line.decode('iso-8859-1')

        # Header lines include the first status line (HTTP/1.x ...).
        # We are going to ignore all lines that don't have a colon in them.
        # This will botch headers that are split on multiple lines...
        if ':' not in header_line:
            return

        # Break the header line into header name and value.
        name, value = header_line.split(':', 1)

        # Strip spaces
        name = name.strip()
        value = value.strip()

        # Header names are case insensitive. Convert to lowercase
        name = name.lower()

        # Save the headers in the dictionary
        headers[name] = value

    try:
        # Create URL from domain and scheme(protocol)
        url = urlunparse((scheme, domain, "", "", "", ""))

        buffer = BytesIO()
        c = pycurl.Curl()
        c.setopt(c.URL, url)

        if http_version == 1:
            c.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_1)
        elif http_version == 2:
            c.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2_0)
        else:
            result.error ="Invalid HTTP version requested"
            return result

        # Follow redirect.
        c.setopt(c.FOLLOWLOCATION, True)
        c.setopt(c.WRITEFUNCTION, buffer.write)
        # Set our header function.
        c.setopt(c.HEADERFUNCTION, header_function)
        #c.setopt(pycurl.VERBOSE, 1)
        c.setopt(c.TIMEOUT, 20)

        #Set User Agent if given
        if user_agent != "":
            c.setopt(pycurl.USERAGENT, user_agent)

        if ipv6:
            # Set the option to force the use of IPv6
            c.setopt(c.IPRESOLVE, c.IPRESOLVE_V6)

        c.perform()

        result.response_code = c.getinfo(c.RESPONSE_CODE)
        result.http_version = c.getinfo(pycurl.INFO_HTTP_VERSION)
        result.total_time = c.getinfo(c.TOTAL_TIME)
        result.headers = headers
        result.content = buffer.getvalue()
        c.close()
        return result

    except Exception as e:
        if ipv6 == False:
            result.error = "[!] Warning: Error connecting to target domain in IPv4 (%s)" % e
            return result
        else:
            result.error = "[!] Warning: Error connecting to target domain in IPv6 (%s)" % e
            return result

