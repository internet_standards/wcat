import plugin
import dns.resolver
import dns.query
import dns.dnssec
import dns.flags

''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin(plugin.APlugin):

    def __init__(self):
        self.name = "DNSSEC"
        self.result = plugin.AResult("DNSSEC")
        self.nameserver = "8.8.8.8"

    '''Checks if the domain (CNAME, A, AAAA) is DNSSEC protected (i.e. the answer is DNSSEC validated)'''
    '''It requires a DNSSEC aware resolver'''
    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):
        try:
            resolver = dns.resolver.Resolver()
            resolver.flags=dns.flags.RD | dns.flags.AD
            resolver.nameservers = [self.nameserver]
            answer = resolver.resolve(domain)
            flags = answer.response.flags
            if dns.flags.AD & flags == 0:
                # Flag AD not set. The domain is not DNSSEC protected
                self.result.supported = False
            else:
                self.result.supported = True
            return [self.result]
        except Exception as e:
            self.result.supported = False
            self.result.error = True
            self.result.log = str(e)
            return [self.result]
