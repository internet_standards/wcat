import plugin
import re

''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin(plugin.APlugin):

    def __init__(self):
        self.name = "Sec Headers"

    def notFoundResult(self, header_name, log):
        result = plugin.AResult("HTTP %s" % header_name)
        result.supported = False
        result.log = log
        return result

    def foundResult(self, header_name, log = None):
        result = plugin.AResult("HTTP %s" % header_name)
        result.supported = True
        result.log = log
        return result


    def assess_hsts(self, headers):
        header = "Strict-Transport-Security"
        header_name = header.lower()
        log = None
        if header_name not in headers.keys():
            return self.notFoundResult(header, "%s header not found." % header_name)

        header_value = headers[header_name]
        hsts_header_regex = re.compile(r'^Strict-Transport-Security:\smax-age=\d+([;\s]\w+(?:-\w+)?(?:="[^"]+")?)*$', re.IGNORECASE)
        if hsts_header_regex.match(header_value) == False:
            log = "Wrong syntax in the HSTS header value"

        return self.foundResult(header, log)

    def assess_x_frame_options(self, headers):
        header =  "X-Frame-Options"
        header_name = header.lower()
        log = None
        if header_name not in headers.keys():
            return self.notFoundResult(header, "%s header not found." % header_name)

        header_value = headers[header_name]
        x_frame_option_regex = re.compile(r'^DENY$|^SAMEORIGIN$|^ALLOW-FROM\s(.+)$', re.IGNORECASE)
        if x_frame_option_regex.match(header_value) == False:
            log = "Wrong syntax in the X-Frame-Options header value"

        return self.foundResult(header, log)

    def assess_x_content_type_options(self, headers):
        header =  "X-Content-Type-Options"
        header_name = header.lower()
        log = None
        if header_name not in headers.keys():
            return self.notFoundResult(header, "%s header not found." % header_name)

        header_value = headers[header_name]
        x_content_type_options_regex = re.compile(r'^[Nn][Oo][Ss][Nn][Ii][Ff][Ff]$', re.IGNORECASE)
        if x_content_type_options_regex.match(header_value) == False:
            log = "Wrong syntax in the X-Frame-Options header value"

        return self.foundResult(header, log)

    def assess_content_security_policy(self, headers):
        header =  "Content-Security-Policy"
        header_name = header.lower()
        log = None
        if header_name not in headers.keys():
            return self.notFoundResult(header, "%s header not found." % header_name)

        header_value = headers[header_name]
        content_security_policy_regex = re.compile(r'^.*(;|$)\s*(default-src\s+.*|script-src\s+.*|object-src\s+.*|style-src\s+.*|img-src\s+.*|media-src\s+.*|frame-src\s+.*|font-src\s+.*|connect-src\s+.*|form-action\s+.*|frame-ancestors\s+.*|plugin-types\s+.*|base-uri\s+.*)\s*$', re.IGNORECASE) 
        if content_security_policy_regex.match(header_value) == False:
            log = "Wrong syntax in the X-Frame-Options header value"

        return self.foundResult(header, log)

    def assess_x_xss_protection(self, headers):
        header =  "X-XSS-Protection"
        header_name = header.lower()
        log = None
        if header_name not in headers.keys():
            return self.notFoundResult(header, "%s header not found." % header_name)

        header_value = headers[header_name]
        x_xss_protection_regex = re.compile(r'^[01](;\s*mode=block)?$', re.IGNORECASE)
        if x_xss_protection_regex.match(header_value) == False:
            log = "Wrong syntax in the X-Frame-Options header value"

        return self.foundResult(header, log)

    def assess_referer_policy(self, headers):
        header =  "Referer-Policy"
        header_name = header.lower()
        log = None
        if header_name not in headers.keys():
            return self.notFoundResult(header, "%s header not found." % header_name)

        header_value = headers[header_name]
        referer_policy_regex = re.compile(r'^no-referrer|no-referrer-when-downgrade|same-origin|origin|strict-origin|strict-origin-when-cross-origin|unsafe-url$', re.IGNORECASE)
        if referer_policy_regex.match(header_value) == False:
            log = "Wrong syntax in the X-Frame-Options header value"

        return self.foundResult(header, log)

    ''' Checks if the domain (CNAME, A, AAAA) is DNSSEC protected (i.e. the answer is DNSSEC validated)'''
    ''' It requires a DNSSEC aware resolver'''
    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):
        results = []
        if hasattr(https1, "headers"):
            results.append(self.assess_hsts(https1.headers))
            results.append(self.assess_x_frame_options(https1.headers))
            results.append(self.assess_x_content_type_options(https1.headers))
            results.append(self.assess_content_security_policy(https1.headers))
            results.append(self.assess_x_xss_protection(https1.headers))
            results.append(self.assess_referer_policy(https1.headers))
        return results
