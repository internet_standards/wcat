import plugin
import os
import subprocess

''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin(plugin.APlugin):
    def __init__(self):
        self.name = "HTTP/3"
        self.result = plugin.AResult("HTTP3")
        if os.path.exists('curl_http3') == False:
            print("[!] ERROR: curl_http3 binary is missing in the local folder!")
            sys.exit(1)

    ''' Parses the Alt-Svc Header. Returns the alt-authority for h3 (if found)'''
    ''' Throughts an exception if there is a parsing error'''
    ''' More information about the Alt-Svc in https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Alt-Svc'''
    def altsvc_parser(self, s):
        # This will keep the parsed structure of several entries separated by commas, containing pairs of K=V separated by ;
        parsed_altsvc = [{}]

        # Store current key
        k = ""
        # Store current value
        v = ""
        # State Machine: 0 = reading key; 1=reading value
        state = 0;
        openquotes = False
        # We read letter by letter
        for c in s:
            # New entry
            if c == "," and openquotes == False:
                parsed_altsvc [-1][k]=v
                parsed_altsvc.append({})
                k = ""
                v = ""
                state = 0;
            # New pair k=v
            elif c == ";" and openquotes == False:
                parsed_altsvc [-1][k]=v
                k = ""
                v = ""
                state = 0;
            elif c == " ":
                pass
            elif c == "\"":
                if openquotes == False:
                    openquotes = True
                else:
                    openquotes = False
            elif c == "=" and openquotes == False and state == 0:
                state = 1;
            else:
                if state == 0: k=k+c
                if state == 1: v=v+c
        # We introduce the last k:v if they exist
        if len(k)>0 and len(v)>0:
            parsed_altsvc [-1][k]=v

        #print(parsed_altsvc)
        if len(parsed_altsvc)>0 and "h3" in parsed_altsvc[0].keys():
            return parsed_altsvc[0]["h3"]
        else:
            return None

    ''' Returns True if a domain advertises HTTP3 support'''
    def assess_http3_connection(self, domain, alt_svc, ipv6=False):

        h3_location = self.altsvc_parser(alt_svc)
        #print(h3_location)
        if h3_location.startswith(":"):
            h3_location = domain + h3_location
        if ipv6 == False:
            return_code = subprocess.call(["./curl_http3", "--http3", "--connect-timeout", "10", "-4", "https://"+h3_location], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        else:
            return_code = subprocess.call(["./curl_http3", "--http3", "--connect-timeout", "10", "-6", "https://"+h3_location], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if return_code == 0:
            return True
        else:
            return False

    ''' Checks if the domain (CNAME, A, AAAA) is DNSSEC protected (i.e. the answer is DNSSEC validated)'''
    ''' It requires a DNSSEC aware resolver'''
    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):
        alt_svc = None
        if hasattr(https1, "headers"):
            if "alt-svc" not in https1.headers.keys():
                self.result.supported = False
                self.result.log = "Alt-Svc header not found"
                return [self.result]
            alt_svc = https1.headers["alt-svc"]
        else:
            self.result.supported = False
            self.result.log = "Alt-Svc header not found"
            return [self.result]

        if self.assess_http3_connection(domain, alt_svc, ipv6=False) == True:
            self.result.supported = True
        else:
            self.result.supported = False

        return [self.result]
