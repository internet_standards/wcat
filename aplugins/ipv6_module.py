import plugin


''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin(plugin.APlugin):

    def __init__(self):
        self.name = "IPv6"
        self.result = plugin.AResult("IPv6")

    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):
        if https1_ipv6.error == None:
            self.result.supported = True
        else:
            self.result.supported = False

        return [self.result]
