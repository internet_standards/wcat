import plugin
import dns.resolver
import subprocess
from itertools import islice
import json

''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin(plugin.APlugin):

    def __init__(self):
        self.name = "RPKI"
        self.result = plugin.AResult("RPKI")

    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):

        records = dns.resolver.resolve(domain)
        for ip in records:
            response = subprocess.run(['whois', '-h', 'whois.cymru.com', f'-v {ip}'], stdout=subprocess.PIPE, text=True)
            #print(f"DEBUG - prefix: {prefix}")
            asn = response.stdout.split('\n')[2].split(' ')[0]
            prefix = response.stdout.split('\n')[2].split('|')[2].split(' ')[1]
            #print(f"DEBUG - asn: {asn}, prefix: {prefix}")
            validator_cmd  = f'curl http://127.0.0.1:8323/api/v1/validity/{asn}/{prefix}'
            process = subprocess.Popen(validator_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
            stout, sterr = process.communicate()
            #print(f"DEBUG: {stout}")
            result = json.loads(stout)
            validity = result["validated_route"]["validity"]["state"]
            #print(f"DEBUG - validity: {validity}")

        if validity.lower() == "valid":
            self.result.supported = True
        else:
            self.result.supported = False

        return [self.result]
