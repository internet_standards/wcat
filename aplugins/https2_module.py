import plugin


''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin(plugin.APlugin):

    def __init__(self):
        self.name = "HTTPS 2.0"
        self.result = plugin.AResult("HTTPS 2.0")

    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):
        if https2.error == None:
            self.result.supported = True
        else:
            self.result.supported = False

        return [self.result]
