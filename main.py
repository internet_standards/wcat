import ssl
from http_engine import run_http_check
from urllib.parse import urlunparse
import sys
from colorama import Fore
import socket
import dns.resolver
import dns.query
import dns.dnssec
import dns.flags
import argparse
import subprocess
import os
import re
from types import SimpleNamespace
import json
from difflib import SequenceMatcher
import importlib

def check_similarity(ipv4_content, ipv6_content):
    return round(SequenceMatcher(None, ipv4_content, ipv6_content).ratio() * 100, 2)

def log(message):
    print(f"INFO: {message}")

'''Class to be used to store each standards to be tested and the results'''
class STest:
    def __init__(self, hashkey, name, header_name = None, skip=False, ipv6 = False):
        self.name = name      # Descriptive name of the Standard
        self.hashkey = hashkey   # Key to be used in dictionaries
        self.skip = skip       # Set to True of the standards will be skipped (not tested)
        self.header_name = header_name # Name of Header if relevant
        self.ipv6 = ipv6       # Set to True if IPv6 will be used
        # Results of the test
        self.supported =  False # True if the standards is supported
        self.ok_support = False # True if no deployment errores were detected.
        self.warnings = ""      # List of warnings in the results

class Result_object:
    def __init__(self, domain, dnssec = None, http3 = None, https1 = None, https2 = None, ipv6 = None, sec_headers = None, rpki = None):
        self.domain = domain
        self.dnssec = dnssec
        self.http3 = http3
        self.https1 = https1
        self.https2 = https2
        self.ipv6 = ipv6
        self.sec_headers = sec_headers
        self.rpki = rpki

'''Checks if the domain (CNAME, A, AAAA) is DNSSEC protected (i.e. the answer is DNSSEC validated)'''
'''It requires a DNSSEC aware resolver'''
def check_dnssec(domain, dnssec=True, nameserver="8.8.8.8"):
    try:
        resolver = dns.resolver.Resolver()
        if dnssec == True:
            resolver.flags=dns.flags.RD | dns.flags.AD
        resolver.nameservers = [nameserver]
        answer = resolver.resolve(domain)
        flags = answer.response.flags
        if dns.flags.AD & flags == 0:
            # Flag AD not set. The domain is not DNSSEC protected
            return False
        else:
            return True
    except:
        return None


''' Function that establishes HTTP, HTTPS 1.0 and HTTPS 2.0 connections in IPv4 and IPv6 '''
''' and it runs all the assessment plugins '''
''' Returns list of results in AResult objects '''
def run_assessment_aplugins(domain, aplugins, user_agent, json = False):
    # Establish HTTP, HTTPS 1.0 and HTTPS 2.0 connections in IPv4
    http = run_http_check (domain, user_agent, scheme="http", http_version = 1, ipv6=False)
    https1 = run_http_check (domain, user_agent, scheme="https", http_version = 1, ipv6=False)
    https2 = run_http_check (domain, user_agent, scheme="https", http_version = 2, ipv6=False)

    # Establish HTTP, HTTPS 1.0 and HTTPS 2.0 connections in IPv4
    http_ipv6 = run_http_check (domain, user_agent, scheme="http", http_version = 1, ipv6=True)
    https1_ipv6 = run_http_check (domain, user_agent, scheme="https", http_version = 1, ipv6=True)
    https2_ipv6 = run_http_check (domain, user_agent, scheme="https", http_version = 2, ipv6=True)

    ipv4_content = http.content if http.error == None else None
    if https2.error == None or https1.error == None:
        ipv4_content = https2.content if https2.error == None else https1.content

    ipv6_content = http_ipv6.content if http_ipv6.error == None else None
    if https2_ipv6.error == None or https1_ipv6.error == None:
        ipv6_content = https2_ipv6.content if https2_ipv6.error == None else https1_ipv6.content

    if json == False and ipv6_content != None and ipv4_content != None:
        ipv4_6_similarity = check_similarity(ipv4_content, ipv6_content)
        print(f"[*] IPv4 - IPv6 content similarity: {ipv4_6_similarity}%")

    aplugins_results = []
    # Run assessment in plugins
    for plugin in aplugins.values():
        aplugins_results.extend( plugin.assessment(domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6) )

    return aplugins_results

''' Prints all assessments'''
def print_aplugin_results(domain, aplugin_results, format_json = False):
    if format_json == False:
        print(f"[*] Results for domain {domain}:")
    else:
        tests = [x.name for x in aplugin_results]
        header_str = "domain"
        for h in tests:
            headers_str = header_str + ", %s" % h
        if format_json == False:
            print(headers_str)

    res_obj = Result_object(domain)
    res_obj.https1 = next((x.__dict__ for x in aplugin_results if x.name == "HTTPS 1.0"), None)
    res_obj.https2 = next((x.__dict__ for x in aplugin_results if x.name == "HTTPS 2.0"), None)
    res_obj.http3 = next((x.__dict__ for x in aplugin_results if x.name == "HTTP3"), None)
    res_obj.ipv6 = next((x.__dict__ for x in aplugin_results if x.name == "IPv6"), None)
    res_obj.dnssec = next((x.__dict__ for x in aplugin_results if x.name == "DNSSEC"), None)
    #res_obj.sec_headers = next((x.__dict__ for x in aplugin_results if "HTTP " in x.name), None)
    res_obj.sec_headers = [x.__dict__ for x in aplugin_results if "HTTP " in x.name]
    res_obj.rpki = [x.__dict__ for x in aplugin_results if "RPKI" in x.name]

    for result in aplugin_results:
        if result.supported == True:
            if result.warning == True:
                supported = Fore.ORANGE +"Supported (with warning)" +Fore.RESET
            else:
                supported = Fore.GREEN +"Supported" +Fore.RESET
        else:
                supported = Fore.RED +"NOT Supported" +Fore.RESET

        if format_json == False:
            print ("  - %s [%s]" % (result.name, supported))

    if format_json == True:
        print(json.dumps(res_obj.__dict__))

    return json.dumps(res_obj.__dict__)

''' Defines user agent mappings'''
def get_user_agent(args):
    user_agents = {
        "chrome": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36",
        "mozilla": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/110.0",
        "edge": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.63",
        "safari": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.3 Safari/605.1.15"
    }

    # Check user agent
    if args.uachrome:
        selected_agent = "chrome"
    elif args.uamozilla:
        selected_agent = "mozilla"
    elif args.uaedge:
        selected_agent = "edge"
    elif args.uasafari:
        selected_agent = "safari"
    else:
        return "", None

    return user_agents[selected_agent], selected_agent.capitalize()


def main():
    # Check that the curl_http3 binary is available
    if os.path.exists('curl_http3') == False:
        print("[!] ERROR: curl_http3 binary is missing in the local folder!")
        sys.exit(1)

    parser = argparse.ArgumentParser()
    parser.add_argument('domain', type=str, help='Domain to assess')
    parser.add_argument('--json', action="store_true", help='Outputs in JSON format (no logs)')
    parser.add_argument('--uachrome', action="store_true", help='Run with Chrome as User Agent')
    parser.add_argument('--uamozilla', action="store_true", help='Run with Mozilla as User Agent')
    parser.add_argument('--uaedge', action="store_true", help='Run with Edge as User Agent')
    parser.add_argument('--uasafari', action="store_true", help='Run with Safari as User Agent')
    args = parser.parse_args()

    # Loading plugins
    if args.json == False:
        print ("Loading plugins:")

    plugins = {}
    plugin_dir = "./aplugins"

    sys.path.append(plugin_dir)
    for plugin_file in sorted(os.listdir(plugin_dir)):
        if re.search('\.py$', plugin_file):
            module_name = re.sub('\.py$', '', plugin_file)
            module = importlib.import_module(module_name)
            if module_name.lower() == "rpki_module": #check routinator installation or disable
                if args.json == False:
                    print("checking routinator installation with default settings, host: 127.0.0.1 port: 8323")
                validator_cmd  = f'curl http://127.0.0.1:8323/api/v1/status'
                process = subprocess.Popen(validator_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
                stout, sterr = process.communicate()
                try:
                    result = json.loads(stout)
                    validity = result["version"]
                    if "routinator" in validity:
                        plugins[module_name] = module.APlugin()
                except:
                    if args.json == False:
                        print("No routinator installation detected. (Perhaps edit host and port details?)")
            else:
                plugins[module_name] = module.APlugin()

    if args.json == False:
        print(f"[*] {len(plugins)} assessment plugins loaded ({','.join(plugins.keys())})")
        print(f"[*] Starting assessment of website: {args.domain}")

    # Test first that the domain resolves at least
    if check_dnssec(args.domain, dnssec=False) == None:
        print(f"[!] ERROR: domain {args.domain} does not resolve")
        sys.exit(1)

    # Set User Agent if given
    user_agent, ua_name = get_user_agent(args)
    if ua_name != None and args.json == False:
        print(f"[*] Selected {ua_name} as User Agent")

    aplugin_results = run_assessment_aplugins (args.domain, plugins, user_agent, args.json)
    print_aplugin_results(args.domain, aplugin_results, format_json = args.json)


if __name__ == '__main__':
    main()
