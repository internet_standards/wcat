# wcat

Web Communications Assessment Tool (WCAT)

## Install

Install python requirements

```
pip install -r requirements.txt
```

### Additional requirements

Additional packages must be installed with the following command:

```
sudo apt-get install python3-dev libcurl4-openssl-dev libssl-dev

```

### RPKI

RPKI plugin is set to work on localhost (127.0.0.1:8323) via the API of the routinator (https://routinator.docs.nlnetlabs.nl/en/stable/installation.html) <br>

WCAT checks for routinator installation (API version endpoint) using the default settings (127.0.0.1:8323). If no installation is found it skips the use of this module.<br>

To change these settings edit the main.py file.

### New modules

New modules in the aplugins folder should be automatically added.

## Usage

How to use it:

```
usage: main.py [-h] [--json] [--uachrome] [--uamozilla] [--uaedge] [--uasafari] domain
```

Example:
```
$ python3 ./main.py google.com

Loading plugins:
[*] 6 assessment plugins loaded (https1,https2,http3,dnssec,ipv6,headers,rpki)
[*] Starting assessment of website: google.com
[*] Results for domain google.com:
  - HTTPS 1.0 [Supported]
  - HTTPS 2.0 [Supported]
  - HTTP3 [Supported]
  - DNSSEC [NOT Supported]
  - IPv6 [Supported]
  - RPKI [Supported]
  - HTTP Strict-Transport-Security [NOT Supported]
  - HTTP X-Frame-Options [Supported]
  - HTTP X-Content-Type-Options [NOT Supported]
  - HTTP Content-Security-Policy [NOT Supported]
  - HTTP X-XSS-Protection [Supported]
  - HTTP Referer-Policy [NOT Supported]

```
Example (json):
```
$ python3 ./main.py google.com --json

{"domain": "google.com", "dnssec": {"name": "DNSSEC", "error": false, "warning": false, "log": "", "supported": false, "ipv6": false}, "http3": {"name": "HTTP3", "error": false, "warning": false, "log": "", "supported": true, "ipv6": false}, "https1": {"name": "HTTPS 1.0", "error": false, "warning": false, "log": "", "supported": true, "ipv6": false}, "https2": {"name": "HTTPS 2.0", "error": false, "warning": false, "log": "", "supported": true, "ipv6": false}, "ipv6": {"name": "IPv6", "error": false, "warning": false, "log": "", "supported": true, "ipv6": false}, "sec_headers": [{"name": "HTTP Strict-Transport-Security", "error": false, "warning": false, "log": "strict-transport-security header not found.", "supported": false, "ipv6": false}, {"name": "HTTP X-Frame-Options", "error": false, "warning": false, "log": null, "supported": true, "ipv6": false}, {"name": "HTTP X-Content-Type-Options", "error": false, "warning": false, "log": "x-content-type-options header not found.", "supported": false, "ipv6": false}, {"name": "HTTP Content-Security-Policy", "error": false, "warning": false, "log": "content-security-policy header not found.", "supported": false, "ipv6": false}, {"name": "HTTP X-XSS-Protection", "error": false, "warning": false, "log": null, "supported": true, "ipv6": false}, {"name": "HTTP Referer-Policy", "error": false, "warning": false, "log": "referer-policy header not found.", "supported": false, "ipv6": false}], "rpki": {"name": "RPKI", "error": false, "warning": false, "log": "", "supported": true, "ipv6": false},}

```
