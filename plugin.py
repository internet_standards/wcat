#!/usr/bin/python3
from http_engine import run_http_check

''' Base class to store the result of the assessment made by the plugin '''
class AResult:
    def __init__(self, name):
        self.name = name
        self.error = False
        self.warning = False
        self.log = ""
        self.supported = False
        self.ipv6 = False

    def __str__ (self):
            return 'AResult(error=' + str(self.error) + ', warning=' + str(self.warning) + ', supported=' + str(self.supported) + ', log=' + str(self.log) + ')'

''' Base class for the plugin system. All plugins in the aplugins folder inherit from this class '''
class APlugin:

    ''' The init function is called when the plugin is loaded at startup'''
    def __init__(self):
        self.name = None
        self.result = AResult()

    ''' The assessment function is called by WCAT to conduct the tests on a given domain'''
    ''' It results an AResult object'''
    def assessment(self, domain, http, https1, https2, http_ipv6, https1_ipv6, https2_ipv6):
        pass

