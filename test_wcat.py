import pytest
import pycurl
import main
import http_engine
import json
import aplugins.http3_module as http3_module
import aplugins.dnssec_module as dnssec_module
import aplugins.ipv6_module as ipv6_module
import aplugins.https1_module as https1_module
import aplugins.https2_module as https2_module
import aplugins.headers_module as headers_module

def validateJSON(jsonData):
    try:
        json.loads(jsonData)
    except ValueError as err:
        return False
    return True

# Pytest functions
def test_check_dnssec():
    assert(main.check_dnssec("google.com") == False)
    assert(main.check_dnssec("ec.europa.eu") == True)
    assert(main.check_dnssec("dnssec-failed.org") == None)

# Define a test case for a valid url and HTTP version
def test_valid_url_and_http_version():
    result = http_engine.run_http_check ("google.com", "", scheme="https", http_version = 2, ipv6=False)
    # Assert that the result is not None
    assert result is not None
    # Assert that the first element of the result is the status line
    assert result.error is None


# Define a test case for an invalid url
def test_invalid_url():
    # Call the function and store the result
    result = http_engine.run_http_check ("invalid.com", "", scheme="https", http_version = 2, ipv6=False)
    # Assert that the result is None
    assert result.error is not None

# Define a test case for an invalid HTTP version
def test_invalid_http_version():
    # Call the function and store the result
    result = http_engine.run_http_check ("google.com", "", scheme="https", http_version = 3, ipv6=False)
    # Assert that the result is None
    assert result.error is not None

def test_similarity():
    # Call the function and store the result
    result = http_engine.run_http_check ("google.com", "", scheme="https", http_version = 2, ipv6=False)
    # Get similarity rating for the same content
    similarity = main.check_similarity(result.content, result.content)
    # Assert that the result is 100% the same
    assert similarity == 100

def test_json_validity():
    # manually set the plugins
    plugins = {}
    plugins["https1"] = https1_module.HTTPS1Module()
    plugins["https2"] = https2_module.HTTPS2Module()
    plugins["http3"] = http3_module.HTTP3Module()
    plugins["dnssec"] = dnssec_module.DNSSECModule()
    plugins["ipv6"] = ipv6_module.IPV6Module()
    plugins["headers"] = headers_module.HeadersModule()

    # Call the function and store the result
    aplugin_results = main.run_assessment_aplugins ("google.com", plugins, "", True)
    json = main.print_aplugin_results("google.com", aplugin_results, format_json = True)

    # Assert json validity
    assert validateJSON(json) == True

